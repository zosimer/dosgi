package dosgi.module.system;

import lib1.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dosgi.module.IModule;
import com.dosgi.module.IModuleContext;

public class SystemModule implements IModule {
	private transient static final Logger LOG = LoggerFactory.getLogger(SystemModule.class);
	private static IModuleContext context;

	public static IModuleContext getContext() {
		return context;
	}

	@Override
	public void init(IModuleContext context) throws Exception {
		SystemModule.context = context;

		//注册bean
		//		context.registry(InterfaceClass, ImplInstance);

		// 处理类信息
		//		Dosgi.context().addClassHandler(new ClassHandler(context) {
		//			@Override
		//			public void handle(Class<?> clazz) throws Exception {
		//				// TODO 处理类信息，例如注解
		//			}
		//		});
		LOG.info("start dosgi.module.system success!");
	}

	@Override
	public void start(IModuleContext context) throws Exception {
		Test.main(null);
		System.out.println("invoke lib success:lib1.Test.main(null)");
	}

	@Override
	public void stop(IModuleContext context) throws Exception {
		SystemModule.context = null;
	}
}
