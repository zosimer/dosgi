package com.dosgi.bean;

import java.util.HashMap;
import java.util.Map;

public class BeanFactoryDefault implements IBeanFactory {
	private Map<Class<?>, Object> beans = new HashMap<Class<?>, Object>();

	@Override
	public void registry(Class<?> clazz, Object bean) {
		beans.put(clazz, bean);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(Class<T> clazz) {
		return (T)beans.get(clazz);
	}

}
