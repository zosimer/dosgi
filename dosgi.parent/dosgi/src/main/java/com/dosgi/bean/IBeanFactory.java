package com.dosgi.bean;

/**
 * bean工厂，用于注册类到实例的缓存
 * 
 */
public interface IBeanFactory {

	/**
	 * 通过类注册
	 * 
	 * @param clazz
	 * @param bean
	 */
	public void registry(Class<?> clazz, Object bean);

	/**
	 * 通过类获取已注册Bean
	 * 
	 * @param clazz
	 * @return
	 */
	public <T> T get(Class<T> clazz);
}
