package com.dosgi;

/**
 * @author dingnate
 *
 */
public enum LifeCycleState {
	INITING, INITED, SCANNING__CLASSES, SCANNED__CLASSES, STARTING, STARTED, CLOSING, CLOSED
}
